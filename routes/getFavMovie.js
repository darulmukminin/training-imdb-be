
var express = require('express');
var router = express.Router();
// import { data_movie } from './data';
var data = require('./dataMovie');
data = data.data
router.get('/', function (req, res, next) {
  data = data.sort(function (a, b) {
    return b.rating - a.rating;
  });
  var response = {
    response: "00",
    data
  }

  res.send(response);
});

module.exports = router;