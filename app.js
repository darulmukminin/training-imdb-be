var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')
var indexRouter = require('./routes/index');
var signinRouter = require('./routes/signin');
var signupRouter = require('./routes/signup');
var newMovieRouter = require('./routes/getNewMovie');
var favMovieRouter = require('./routes/getFavMovie');
var carouselRouter = require('./routes/getCarousel');
var searchMovieRouter = require('./routes/getSearchMovie');
var addMovie = require('./routes/addMovie');
var editMovie = require('./routes/editMovie');
var deleteMovie = require('./routes/deleteMovie');
var getWishlist = require('./routes/getWhistlist');
var addWishlist = require('./routes/addWishlist');


var app = express();
app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/signin', signinRouter);
app.use('/signup', signupRouter);
app.use('/newmovie', newMovieRouter);
app.use('/favmovie', favMovieRouter);
app.use('/carousel', carouselRouter);
app.use('/search', searchMovieRouter);
app.use('/addmovie', addMovie);
app.use('/editmovie', editMovie);
app.use('/deletemovie', deleteMovie);
app.use('/getwishlist', getWishlist);
app.use('/addwishlist', addWishlist);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
